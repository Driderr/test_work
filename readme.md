install NodeJS https://nodejs.org/en/download/

##Dev start on localhost
From project root run:
```
	$ npm i
```

For Not-Windows run
```
	$ npm run start
```

For Windows run
```
	$ npm run start-win
```
Check project: [http://localhost:8080/](http://localhost:8080/)


##Build dist
From project root run:
```
	$ npm i
```

For Not-Windows run
```
	$ npm run build
```

For Windows run
```
	$ npm run build-win
```

For start project
```
	$ npm run server
```
Check project: [http://localhost:3000/](http://localhost:3000/ "")
