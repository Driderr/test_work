const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req, res) => {
	res.sendFile(`${__dirname}/dist/index.html`);
});

http.createServer(app).listen(app.get('port'), () => {
	console.log(`myApp server listening on port ${app.get('port')}`);
});
